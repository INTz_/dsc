﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DSC.Models;

namespace DSC.Controllers
{
    public class ContractorsAreaController : Controller
    {
        public IActionResult Trainingcourses()
        {
            ViewData["Message"] = "Trainingcourses.";
            return View();
        }

        public IActionResult VideoTutorial()
        {
            ViewData["Message"] = "VideoTutorial.";

            return View();
        }

        public IActionResult ImaginationSoftware()
        {
            ViewData["Message"] = "ImaginationSoftware.";

            return View();
        }

        public IActionResult Catalogues()
        {
            ViewData["Message"] = "Catalogues.";
            return View();
        }

        public IActionResult TechnicalDocumentation()
        {
            ViewData["Message"] = "TechnicalDocumentation.";
            return View();
        }

        public IActionResult Download()
        {
            ViewData["Message"] = "Download.";
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
