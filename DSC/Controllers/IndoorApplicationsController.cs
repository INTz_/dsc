﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DSC.Models;

namespace DSC.Controllers
{
    public class IndoorApplicationsController : Controller
    {
        public IActionResult bathroom()
        {
            ViewData["Message"] = "bathroom";
            return View();
        }
        public IActionResult bedrooms()
        {
            ViewData["Message"] = "bedrooms";
            return View();
        }
        public IActionResult feed()
        {
            ViewData["Message"] = "feed";
            return View();
        }
        public IActionResult furniture()
        {
            ViewData["Message"] = "furniture";
            return View();
        }
        public IActionResult kitchen()
        {
            ViewData["Message"] = "kitchen";
            return View();
        }
        public IActionResult living()
        {
            ViewData["Message"] = "living";
            return View();
        }
        public IActionResult museums()
        {
            ViewData["Message"] = "museums";
            return View();
        }
        public IActionResult offices()
        {
            ViewData["Message"] = "offices";
            return View();
        }
        public IActionResult publicplaces()
        {
            ViewData["Message"] = "publicplaces";
            return View();
        }
        public IActionResult restaurant()
        {
            ViewData["Message"] = "restaurant";
            return View();
        }
        public IActionResult retail()
        {
            ViewData["Message"] = "retail";
            return View();
        }
        public IActionResult staircases()
        {
            ViewData["Message"] = "staircases";
            return View();
        }
        public IActionResult walls()
        {
            ViewData["Message"] = "walls";
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
