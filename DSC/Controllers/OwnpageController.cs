﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DSC.Models;

namespace DSC.Controllers
{
    public class OwnpageController : Controller
    {
        public IActionResult FireandconcreteBurnoutbyFrancescoFeltrin()
        {
            ViewData["Message"] = "FireandconcreteBurnoutbyFrancescoFeltrin.";
            return View();
        }

        public IActionResult inspiringinnovationsarebreathingnewlifeintoanageoldflooringtechnique()
        {
            ViewData["Message"] = "inspiringinnovationsarebreathingnewlifeintoanageoldflooringtechnique.";
            return View();
        }

        public IActionResult idealworkresidentialprojects()
        {
            ViewData["Message"] = "idealworkresidentialprojects.";
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
