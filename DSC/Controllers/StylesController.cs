﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DSC.Models;

namespace DSC.Controllers
{
    public class StylesController : Controller
    {
        public IActionResult classicalcontemporary()
        {
            ViewData["Message"] = "classicalcontemporary";
            return View();
        }
        public IActionResult colourful()
        {
            ViewData["Message"] = "colourful";
            return View();
        }
        public IActionResult eclectic()
        {
            ViewData["Message"] = "eclectic";
            return View();
        }
        public IActionResult feed()
        {
            ViewData["Message"] = "feed";
            return View();
        }
        public IActionResult industrial()
        {
            ViewData["Message"] = "industrial";
            return View();
        }
        public IActionResult minimal()
        {
            ViewData["Message"] = "minimal";
            return View();
        }
       
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
