﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DSC.Models;

namespace DSC.Controllers
{
    public class RenovationsController : Controller
    {
        public IActionResult IndoorRenovation()
        {
            ViewData["Message"] = "IndoorRenovation.";
            return View();
        }

        public IActionResult OutdoorRenovation()
        {
            ViewData["Message"] = "OutdoorRenovation.";

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
