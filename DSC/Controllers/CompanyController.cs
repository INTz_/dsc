﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DSC.Models;

namespace DSC.Controllers
{
    public class CompanyController : Controller
    {
        public IActionResult Company()
        {
            ViewData["Message"] = "Company.";
            return View();
        }

        public IActionResult IdealNews()
        {
            ViewData["Message"] = "IdealNews.";

            return View();
        }

        public IActionResult UnrepeatableBeauty()
        {
            ViewData["Message"] = "UnrepeatableBeauty.";

            return View();
        }

        public IActionResult Contacts()
        {
            ViewData["Message"] = "Contacts.";
            return View();
        }
        
        public IActionResult SocialWall()
        {
            ViewData["Message"] = "SocialWall.";
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
