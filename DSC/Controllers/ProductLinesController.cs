﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DSC.Models;

namespace DSC.Controllers
{
    public class ProductLinesController : Controller
    {
        public IActionResult AcidStainedFloor()
        {
            ViewData["Message"] = "AcidStainedFloor.";
            return View();
        }

        public IActionResult IdealTix()
        {
            ViewData["Message"] = "IdelTix.";

            return View();
        }

        public IActionResult IdealWall()
        {
            ViewData["Message"] = "IdealWall.";

            return View();
        }

        public IActionResult Lixio()
        {
            ViewData["Message"] = "Lixio.";
            return View();
        }

        public IActionResult LixioPlus()
        {
            ViewData["Message"] = "LixioPlus.";
            return View();
        }

        public IActionResult Microtopping()
        {
            ViewData["Message"] = "Microtopping.";
            return View();
        }

        public IActionResult MicrotoppingWallTexture()
        {
            ViewData["Message"] = "MicrotoppingWallTexture.";
            return View();
        }

        public IActionResult NuvolatoArchitop()
        {
            ViewData["Message"] = "NuvolatoArchitop.";
            return View();
        }

        public IActionResult Purometallo()
        {
            ViewData["Message"] = "Purometallo.";
            return View();
        }

        public IActionResult Rasico()
        {
            ViewData["Message"] = "Rasico.";
            return View();
        }

        public IActionResult SassoitaliaFloor()
        {
            ViewData["Message"] = "SassoitaliaFloor.";
            return View();
        }

        public IActionResult StampedConcrete()
        {
            ViewData["Message"] = "StampedConcrete.";
            return View();
        }

        public IActionResult StenciltopFloor()
        {
            ViewData["Message"] = "StenciltopFloor.";
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
