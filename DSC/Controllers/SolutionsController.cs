﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DSC.Models;

namespace DSC.Controllers
{
    public class SolutionsController : Controller
    {
        public IActionResult Typologies()
        {
            ViewData["Message"] = "Typologies.";
            return View();
        }

        public IActionResult IndoorApplications()
        {
            ViewData["Message"] = "IndoorApplications.";

            return View();
        }

        public IActionResult OutdoorApplications()
        {
            ViewData["Message"] = "OutdoorApplications.";

            return View();
        }

        public IActionResult Styles()
        {
            ViewData["Message"] = "Styles.";
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
