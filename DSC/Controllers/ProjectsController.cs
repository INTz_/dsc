﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DSC.Models;
using Microsoft.AspNetCore.Routing;

namespace DSC.Controllers
{
    public class ProjectsController : Controller
    {
        [Route("filter")]
        public ActionResult Projects(string filter)
        {
            ViewBag.filter = filter;
            return View();
        }

        [Route("page")]
        public ActionResult Pages(string page)
        {
            return View("~/Views/Projects/" + page+".cshtml");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
