﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DSC.Models;

namespace DSC.Controllers
{
    public class TypologiesController : Controller
    {
        public IActionResult floors()
        {
            ViewData["Message"] = "floors.";
            return View();
        }
        public IActionResult coatings()
        {
            ViewData["Message"] = "coatings.";
            return View();
        }
        public IActionResult flooringcoatings()
        {
            ViewData["Message"] = "flooringcoatings.";
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
