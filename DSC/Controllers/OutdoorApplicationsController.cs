﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DSC.Models;

namespace DSC.Controllers
{
    public class OutdoorApplicationsController : Controller
    {
        public IActionResult amusementparks()
        {
            ViewData["Message"] = "amusementparks";
            return View();
        }
        public IActionResult carparks()
        {
            ViewData["Message"] = "carparks";
            return View();
        }
        public IActionResult feed()
        {
            ViewData["Message"] = "feed";
            return View();
        }
        public IActionResult outdoorliving()
        {
            ViewData["Message"] = "outdoorliving";
            return View();
        }
        public IActionResult publicareas()
        {
            ViewData["Message"] = "publicareas";
            return View();
        }
        public IActionResult ramps()
        {
            ViewData["Message"] = "ramps";
            return View();
        }
        public IActionResult squares()
        {
            ViewData["Message"] = "squares";
            return View();
        }
        public IActionResult swimmingpools()
        {
            ViewData["Message"] = "swimmingpools";
            return View();
        }
        public IActionResult walkways()
        {
            ViewData["Message"] = "walkways";
            return View();
        }
        public IActionResult walls()
        {
            ViewData["Message"] = "walls";
            return View();
        }
       
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
